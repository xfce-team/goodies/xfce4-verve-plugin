# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ápo. <apostolos.papadimitriu@gmail.com>, 2016
# Christos Arvanitis <arvchristos@gmail.com>, 2019
# Efstathios Iosifidis <iefstathios@gmail.com>, 2015
# Stavros Giannouris <stavrosg@serverhive.com>, 2006
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-08 00:52+0200\n"
"PO-Revision-Date: 2013-07-03 19:26+0000\n"
"Last-Translator: Christos Arvanitis <arvchristos@gmail.com>, 2019\n"
"Language-Team: Greek (http://app.transifex.com/xfce/xfce-panel-plugins/language/el/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/verve.c:102
#, c-format
msgid "Could not execute command (exit status %d)"
msgstr "Αδυναμία εκτέλεσης της εντολής (exit status %d)"

#. Generate error message
#: ../panel-plugin/verve-plugin.c:389
msgid "Could not execute command:"
msgstr "Αδυναμία εκτέλεσης της εντολής:"

#. Create properties dialog
#: ../panel-plugin/verve-plugin.c:1137
msgid "Verve"
msgstr "Verve"

#: ../panel-plugin/verve-plugin.c:1140
msgid "_Close"
msgstr "_Κλείσιμο"

#: ../panel-plugin/verve-plugin.c:1160
msgid "General"
msgstr "Γενικά"

#. Frame for appearance settings
#: ../panel-plugin/verve-plugin.c:1165
msgid "Appearance"
msgstr "Εμφάνιση"

#. Plugin size label
#: ../panel-plugin/verve-plugin.c:1181
msgid "Width (in chars):"
msgstr "Πλάτος (χαρακτήρες):"

#. Plugin label label
#: ../panel-plugin/verve-plugin.c:1206
msgid "Label:"
msgstr "Ετικέτα:"

#. Frame for color settings
#: ../panel-plugin/verve-plugin.c:1223
msgid "Colors"
msgstr "Χρώματα"

#. Plugin background color label
#: ../panel-plugin/verve-plugin.c:1239
msgid "Background color:"
msgstr "Χρώμα φόντου:"

#. Plugin foreground color label
#: ../panel-plugin/verve-plugin.c:1265
msgid "Text color:"
msgstr "Χρώμα κειμένου:"

#. Frame for behaviour settings
#: ../panel-plugin/verve-plugin.c:1286
msgid "History"
msgstr "Ιστορικό"

#. History length label
#: ../panel-plugin/verve-plugin.c:1302
msgid "Number of saved history items:"
msgstr "Μέγεθος λίστας ιστορικού:"

#. Second tab
#: ../panel-plugin/verve-plugin.c:1322 ../panel-plugin/verve-plugin.c:1326
msgid "Behaviour"
msgstr "Συμπεριφορά"

#. Pattern types frame label
#: ../panel-plugin/verve-plugin.c:1335
msgid "Enable support for:"
msgstr "Ενεργοποίηση υποστήριξης για:"

#. Command type: URL
#: ../panel-plugin/verve-plugin.c:1341
msgid "URLs (http/https/ftp/ftps)"
msgstr "URLs (http/https/ftp/ftps)"

#. Command type: email address
#: ../panel-plugin/verve-plugin.c:1349
msgid "Email addresses"
msgstr "Διευθύνσεις ηλεκτρονικής αλληλογραφίας"

#. Command type: directory path
#: ../panel-plugin/verve-plugin.c:1357
msgid "Directory paths"
msgstr "Διαδρομές καταλόγων"

#. wordexp checkbox
#: ../panel-plugin/verve-plugin.c:1365
msgid "Expand variables with wordexp"
msgstr "Επέκταση μεταβλητών με το wordexp"

#. Command type: !bang
#: ../panel-plugin/verve-plugin.c:1376
msgid "DuckDuckGo queries (starting with !)"
msgstr "Ερωτήματα DuckDuckGo (ξεκινούν με !)"

#. Command type: I'm feeling ducky (\)
#: ../panel-plugin/verve-plugin.c:1384
msgid "DuckDuckGo queries (starting with \\)"
msgstr "Ερωτήματα DuckDuckGo (ξεκινούν με \\)"

#. Fallback if the above don't match
#: ../panel-plugin/verve-plugin.c:1392
msgid "If the above patterns don't match:"
msgstr "Εάν δεν ταιριάζουν τα παραπάνω μοτίβα:"

#. Smart bookmark radio button
#: ../panel-plugin/verve-plugin.c:1398
msgid "Use smart bookmark URL"
msgstr "Χρήση έξυπνων σελιδοδεικτών URL"

#. Executable command radio button (smart bookmark off)
#: ../panel-plugin/verve-plugin.c:1420
msgid "Run as executable command"
msgstr "Εκτέλεση ως εκτελέσιμης εντολής"

#. Use shell checkbox
#: ../panel-plugin/verve-plugin.c:1427
msgid ""
"Run command with $SHELL -i -c\n"
"(enables alias and variable expansion)"
msgstr "Εκτέλεση εντολής με $SHELL -i -c\n(ενεργοποίηση alias και μεταβλητής επέκτασης)"

#. vim:set expandtab sts=2 ts=2 sw=2:
#: ../panel-plugin/xfce4-verve-plugin.desktop.in.h:1
msgid "Verve Command Line"
msgstr "Γραμμή Εντολών Verve"

#: ../panel-plugin/xfce4-verve-plugin.desktop.in.h:2
msgid "Command line interface with auto-completion and command history"
msgstr "Γραμμή εντολών με αυτόματη συμπλήρωση και ιστορικό"
